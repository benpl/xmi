package core;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.Command;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * Created by Ben on 02-Jun-17.
 */
public class IRCListener extends ListenerAdapter
{
    private final XMIBot xmi;

    private String PREFIX = ".";

    public IRCListener(final XMIBot xmi)
    {
        this.xmi = xmi;
    }

    @Override
    public void onMessage(MessageEvent channelMessage)
    {
        if (channelMessage.getMessage().startsWith(this.PREFIX))
        {
            String commandName = channelMessage.getMessage().split(" ")[0].substring(1);

            if (xmi.getCommands().containsKey(commandName))
            {
                Command command = xmi.getCommands().get(commandName);
                if (command.requiresOp() && !channelMessage.getUser().isIrcop())
                {
                    channelMessage.respond("You must be a server oper to execute this command!");
                    return;
                }
                else if (command.isEnabled())
                {
                    command.execute(channelMessage);
                    xmi.getLogger().info(commandName + " Command executed.");
                    return;
                }
            }

            xmi.getCommands().forEach((name,command) ->
            {
                if (command.getAliases() != null)
                    if (command.getAliases().contains(commandName))
                    {
                        if (command.requiresOp() && !channelMessage.getUser().isIrcop())
                        {
                            channelMessage.respond("You must be a server oper to execute this command!");
                            return;
                        }
                        else if (command.isEnabled())
                        {
                            command.execute(channelMessage);
                            xmi.getLogger().info(name + " Command executed.");
                            return;
                        }
                        return;
                    }
                    else
                    {
                        xmi.getLogger().info("No aliases found.");
                    }
            });
        }
    }
}
