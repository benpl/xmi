package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ben on 02-Jun-17.
 */
public class LoadModuleCommand extends GenericCommand
{
    public LoadModuleCommand(final XMIBot xmi, Module module)
    {
        super(xmi, module);
        super.setName("load");
        super.setDescription("load <module name>, ... Loads modules from their name in module data. " +
                "Modules are loaded from the module directory only.");
        super.setAliases(new ArrayList<String>()
        {{
            add("loadmod");
        }});
        super.requiresOp(true);
    }

    @Override
    public void execute(MessageEvent event)
    {
        getBot().getIrcBot().send().message(event.getChannelSource(), "Attempting to load module(s)");
        String[] modules = event.getMessage().substring(event.getMessage().indexOf(" ") + 1).split(" ");
        execute(modules);
    }

    public void execute(String... args)
    {
        getBot().getLogger().info("Attempting to load module(s) " + Arrays.toString(args));
        getBot().getModuleManager().loadModule(args);
    }
}

