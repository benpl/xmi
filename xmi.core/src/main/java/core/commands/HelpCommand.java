package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 09-Jun-17.
 */
public class HelpCommand extends GenericCommand
{
    public HelpCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("help");
        super.setDescription("help <command> to display the commands description.");
    }

    private List<String> helpList()
    {
        List<String> toReturn = new ArrayList<>();

        getBot().getCommands().forEach((name, command)->
        {
            if (command.getDescription() != null)
                toReturn.add(name + ": " + command.getDescription());
            else
                toReturn.add(name + ": Command has no description!");
        });

        return toReturn;
    }

    private String helpCommand(String command)
    {
        return getBot().getCommands().get(command).getDescription();
    }

    @Override
    public void execute(String... args)
    {
        for (String arg : args)
        {
            if (getBot().getCommands().containsKey(arg))
            {

            }
        }
    }

    public void execute(MessageEvent event)
    {
        String[] args = event.getMessage().split(" ");
        if (args.length == 1)
        {
            List<String> messages = helpList();
            messages.forEach(message -> getBot().getIrcBot().send().message(event.getChannelSource(), message));
        }
        else
        {
            getBot().getIrcBot().send().message(event.getChannelSource(), helpCommand(args[1]));
        }
    }
}
