package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.Arrays;

/**
 * Created by Ben on 2017-06-21.
 */
public class UnloadModuleCommand extends GenericCommand
{
    public UnloadModuleCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.requiresOp();
        super.setName("unload");
        super.setAliases(Arrays.asList("remove","deload"));
    }

    public void execute(String... args)
    {
        getBot().getModuleManager().unLoadModule(args);
        getBot().getLogger().info("Unload Finished");
    }

    public void execute(MessageEvent event)
    {
        getBot().getIrcBot().send().message(event.getChannelSource(), "Attempting to unload modules");
        String[] modules = event.getMessage().substring(event.getMessage().indexOf(" ") + 1).split(" ");
        execute(modules);
    }
}
