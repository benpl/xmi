package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * Created by Ben on 2017-06-21.
 */
public class DisableCommand extends GenericCommand
{
    public DisableCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("disable");
        super.requiresOp();
        super.setDescription("Usage: enable <command name>");
    }

    public void execute(String... args)
    {
        for (String command : args)
        {
            getBot().getCommandManager().setEnabledCommand(command, false);
        }
    }

    public void execute(MessageEvent event)
    {
        String[] commands = event.getMessage().substring(event.getMessage().indexOf(" ")).split(" ");
        execute(commands);
        getBot().getIrcBot().send().message(event.getChannelSource(), "Disabled Commands");
    }
}
