package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * Created by Ben on 06-Jun-17.
 */
public class PingCommand extends GenericCommand
{
    public PingCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("ping");
        super.setDescription("calculate time between message send and reply time.");
    }

    @Override
    public void execute(MessageEvent event)
    {
        long initialTime = event.getTimestamp();
        long processTime = System.currentTimeMillis();

        getBot().getIrcBot().send().message(event.getChannelSource(), "Pong: " + (processTime-initialTime) + " ms");

    }
}
