package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;

import java.util.ArrayList;

/**
 * Created by Ben on 2017-06-20.
 */
public class RestartCommand extends GenericCommand
{
    public RestartCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("restart");
        super.requiresOp();
        super.setDescription("Attempts to restart the connection");
        super.setAliases(new ArrayList()
        {{
            add("reload");
            add("rehash");
        }});
    }

    public void execute()
    {
        getBot().restart();
    }
}
