package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.Arrays;

/**
 * Created by Ben on 09-Jun-17.
 */
public class StopCommand extends GenericCommand
{
    public StopCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("shutdown");
        super.requiresOp(true);
        super.setDescription("shutdown the bot.");
        super.setAliases(Arrays.asList("stop"));
    }

    @Override
    public void execute()
    {
        getBot().shutdown();
    }

    @Override
    public void execute(MessageEvent event)
    {
        getBot().shutdown();
    }
}
