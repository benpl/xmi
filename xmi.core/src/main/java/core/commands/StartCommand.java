package core.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;

/**
 * Created by Ben on 08-Jun-17.
 */
public class StartCommand extends GenericCommand
{
    public StartCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("start");
        super.requiresOp(true);
        super.setDescription("starts up the bot, deprecating soon.");
    }

    @Override
    public void execute()
    {
        getBot().start();
    }
}
