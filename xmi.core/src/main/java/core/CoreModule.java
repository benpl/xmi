package core;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.Command;
import ca.benpl.xmi.module.AbstractModule;
import core.commands.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by benpl on 2017-06-22.
 */
public class CoreModule extends AbstractModule
{

        private Map<String, Command> consoleCommands = new ConcurrentHashMap<>();

        private ConsoleListener listener;
        private Thread consoleThread;

        public CoreModule(final XMIBot xmi)
        {
            super(xmi);
            listener = new ConsoleListener(getBot());
            consoleThread = new Thread(listener);
            consoleThread.setName("Console Listener");
        }

        public void load()
        {
            addCommand(new StartCommand(getBot(), this));
            addCommand(new StopCommand(getBot(), this));
            addCommand(new RestartCommand(getBot(), this));
            addCommand(new LoadModuleCommand(getBot(), this));
            addCommand(new PingCommand(getBot(), this));
            addCommand(new HelpCommand(getBot(), this));
            addCommand(new UnloadModuleCommand(getBot(), this));
            addCommand(new EnableCommand(getBot(), this));
            addCommand(new DisableCommand(getBot(), this));

            getBot().addIRCListener(new IRCListener(getBot()));

            consoleThread.start();
        }

        public void unLoad()
        {
            listener.stop();
        }
}