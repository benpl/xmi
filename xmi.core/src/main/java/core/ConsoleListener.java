package core;

import ca.benpl.xmi.XMIBot;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Ben on 02-Jun-17.
 */
public class ConsoleListener implements Runnable
{
    private final XMIBot xmi;
    private final Scanner scanner;

    volatile boolean running;

    public ConsoleListener(final XMIBot xmi)
    {
        this.xmi = xmi;
        this.scanner = new Scanner(System.in);
        running = true;
    }

    void stop()
    {
        this.running = false;
    }

    @Override
    public void run()
    {
        while (running)
        {
            String input = scanner.nextLine();

            String[] args = input.split(" ");

            if (xmi.getCommands().containsKey(args[0]))
            {
                if (args.length > 1)
                    xmi.getCommands().get(args[0]).execute(Arrays.copyOfRange(args, 1, args.length));
                else
                    xmi.getCommands().get(args[0]).execute();
            }
        }
    }
}
