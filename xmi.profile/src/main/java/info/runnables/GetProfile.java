package info.runnables;

import com.lambdaworks.redis.RedisClient;
import info.InfoWriter;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * Created by Ben on 2017-06-19.
 */
public class GetProfile implements Callable
{
    private RedisClient client;
    private String user;
    public GetProfile(RedisClient client, String user)
    {
        this.client = client;
        this.user = user;
    }

    @Override
    public String call()
    {
        try {
            List<String> profile = new InfoWriter(client).getProfile(user).get();
            return user+ ": " + profile.get(0) + "; " + profile.get(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return user + ": empty";
    }
}
