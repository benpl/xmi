package info.runnables;

import com.lambdaworks.redis.RedisClient;
import info.InfoWriter;

/**
 * Created by Ben on 2017-06-19.
 */
public class WriteTitle implements Runnable
{
    private RedisClient client;
    private String user, message;
    public WriteTitle(RedisClient client, String user, String message)
    {
        this.client = client;
        this.user = user;
        this.message = message;
    }

    @Override
    public void run()
    {
        new InfoWriter(client).setTitle(user, message);
    }
}
