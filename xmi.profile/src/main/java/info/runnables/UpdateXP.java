package info.runnables;

import com.lambdaworks.redis.RedisClient;
import info.LevelTracker;

/**
 * Created by Ben on 2017-06-19.
 */
public class UpdateXP implements Runnable
{
    private RedisClient client;
    private String user;
    public UpdateXP(RedisClient client, String user)
    {
        this.client = client;
        this.user = user;
    }

    @Override
    public void run()
    {
        new LevelTracker(client).updateXP(user);
    }
}
