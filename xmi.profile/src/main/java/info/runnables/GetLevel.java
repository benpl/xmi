package info.runnables;

import com.lambdaworks.redis.RedisClient;
import info.LevelTracker;

import java.util.concurrent.Callable;

/**
 * Created by Ben on 2017-06-19.
 */
public class GetLevel implements Callable
{
    private RedisClient client;
    private String user;
    public GetLevel(final RedisClient client, String user)
    {
        this.client = client;
        this.user = user;
    }

    @Override
    public Integer call()
    {
        return Integer.parseInt(new LevelTracker(client).getLevel(user).get(0));
    }
}
