package info.runnables;

import com.lambdaworks.redis.RedisClient;
import info.InfoWriter;

/**
 * Created by Ben on 2017-06-19.
 */
public class WriteInfo implements Runnable
{
    private RedisClient client;
    String user, message;
    public WriteInfo(RedisClient client, String user, String message)
    {
        this.client = client;
        this.user = user;
        this.message = message;
    }

    @Override
    public void run()
    {
        new InfoWriter(client).setInfo(user, message);
    }
}
