package info.runnables;

import com.lambdaworks.redis.RedisClient;
import info.LevelTracker;

import java.util.concurrent.Callable;

/**
 * Created by Ben on 2017-06-21.
 */
public class GetXP implements Callable
{
    private RedisClient client;
    private String user;
    public GetXP(RedisClient client, String user)
    {
        this.client = client;
        this.user = user;
    }

    @Override
    public Long call() throws Exception
    {
        return Long.parseLong(new LevelTracker(client).getXP(user).get(0));
    }
}
