package info;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;


/**
 * Created by Ben on 25-May-17.
 */
public class InfoWriter
{
    private RedisAsyncCommands commands;

    private HashMap<String, String> vals = new HashMap<>();

    public InfoWriter(RedisClient client)
    {
        commands = client.connect().async();
    }

    private void updateTable(String user)
    {
        commands.hmset(InfoModule.DB_NAME + user, vals);
    }

    public void setInfo(String user, String message)
    {
        vals.put("info", message);
        updateTable(user);
    }

    public void setTitle(String user, String title)
    {
        vals.put("title", title);
        updateTable(user);
    }

    public Future<List<String>> getProfile(String user)
    {
        return commands.hmget(InfoModule.DB_NAME+user,"title", "info");
    }
}