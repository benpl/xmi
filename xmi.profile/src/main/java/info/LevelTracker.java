package info;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.sync.RedisCommands;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Ben on 28-May-17.
 */
public class LevelTracker
{
    private RedisCommands<String, String> commands;

    public final static long COOLDOWN_TIME = 60000; // 1 minute, 60,000 milliseconds

    public LevelTracker(RedisClient client)
    {
        commands = client.connect().sync();
    }

    private boolean cooldownExpired(long creationTime, long databaseTime)
    {
        return ((creationTime - databaseTime) > COOLDOWN_TIME);
    }

    private long getXP(long time, long xpBefore)
    {
        return new Random(time).nextInt(25) + 15 + xpBefore;
    }

    private int calculateLevel(long xp)
    {
        return  (int)(15 * Math.log10((xp / 1000.0) + 1.0));
    }

    public void updateXP(String user)
    {
        long currentTime = System.currentTimeMillis();
        long previousTime =  (commands.hexists(InfoModule.DB_NAME+user,"cooldownTime")) ? Long.parseLong(commands.hmget(InfoModule.DB_NAME+user,"cooldownTime").get(0)) : 0;

        if (cooldownExpired(currentTime, previousTime))
        {
            long previousXP = (commands.hexists(InfoModule.DB_NAME+user,"xp")) ? Long.parseLong(commands.hmget(InfoModule.DB_NAME+user,"xp").get(0)) : 0;
            long xp = getXP(currentTime, previousXP);
            int lvl = calculateLevel(xp);

            commands.hmset(InfoModule.DB_NAME+user, new HashMap<String, String>()
                    {{
                        put("xp", String.valueOf(xp));
                        put("level", String.valueOf(lvl));
                        put("cooldownTime", String.valueOf(currentTime));
                    }}
            );
        }
    }

    public List<String> getLevel(String user)
    {
        return commands.hmget(InfoModule.DB_NAME+user, "level");
    }

    public List<String> getXP(String user)
    {
        return commands.hmget(InfoModule.DB_NAME+user, "xp");
    }
}
