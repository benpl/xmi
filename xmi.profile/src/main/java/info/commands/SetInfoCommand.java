package info.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import info.runnables.WriteInfo;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;

/**
 * Created by Ben on 05-Jun-17.
 */
public class SetInfoCommand extends GenericCommand
{
    public SetInfoCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("add");
        super.setDescription("Usage: Add <info message here>. Sets your info message.");
        super.setAliases(new ArrayList<String>()
        {{
            add("set");
            add("addinfo");
        }});
    }

    @Override
    public void execute(MessageEvent event)
    {
        String infoMessage = event.getMessage().substring(event.getMessage().indexOf(" ")+1);
        super.getParent().getBot().getExecutor().execute(new WriteInfo(getBot().getDbClient(), event.getUserHostmask().getNick(), infoMessage));
        super.getBot().getIrcBot().send().message(event.getChannelSource(), "Info set: " + infoMessage);
    }
}
