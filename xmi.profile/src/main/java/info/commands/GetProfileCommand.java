package info.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import info.InfoModule;
import info.runnables.GetProfile;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Ben on 30-May-17.
 */
public class GetProfileCommand extends GenericCommand
{
    public GetProfileCommand(final XMIBot xmi, final InfoModule module)
    {
        super(xmi, module);
        super.setName("profile");
        super.setDescription("Usage: profile <nick>. Shows the title of the user followed by their description.");
        super.setAliases(new ArrayList<String>()
        {{
            add("info");
            add("prof");
            add("bio");
        }});

    }

    @Override
    public void execute(MessageEvent event)
    {
        Future<String> message = getBot().getExecutor().submit(new GetProfile(getBot().getDbClient(), event.getUser().getNick()));
        try {
            getBot().getIrcBot().send().message(event.getChannelSource(), message.get(5, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e)
        {
            e.printStackTrace();
        }
    }
}

