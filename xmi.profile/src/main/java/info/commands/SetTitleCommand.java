package info.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import info.runnables.WriteTitle;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;

/**
 * Created by Ben on 06-Jun-17.
 */
public class SetTitleCommand extends GenericCommand
{
    public SetTitleCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("title");
        super.setDescription("Usage: title <title>. Sets your title.");
        super.setAliases(new ArrayList<String>()
        {{
            add("settitle");
            add("addtitle");
        }});
    }

    @Override
    public void execute(MessageEvent event)
    {
        String title = event.getMessage().substring(event.getMessage().indexOf(" ")+1);
        getBot().getExecutor().execute(new WriteTitle(getBot().getDbClient(), event.getUserHostmask().getNick(), title));
        getBot().getIrcBot().send().message(event.getChannelSource(), "Title: " + title + " set for user: " + event.getUser().getNick());
    }
}
