package info.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import info.runnables.GetXP;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Ben on 2017-06-21.
 */
public class GetXPCommand extends GenericCommand
{
    public GetXPCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("xp");
        super.setDescription("Usage: xp |<user> get ammount of XP you or another user has.");
    }

    @Override
    public void execute(MessageEvent event)
    {
        String[] message = event.getMessage().split(" ");
        try
        {
            Future<Long> xp;
            if (message.length > 1)
            {
                xp = getBot().getExecutor().submit(new GetXP(getBot().getDbClient(), message[1]));
            }
            else
            {
                xp = getBot().getExecutor().submit(new GetXP(getBot().getDbClient(), event.getUser().getNick()));
            }
            getBot().getIrcBot().send().message(event.getChannelSource(), "XP: " + xp.get(5, TimeUnit.SECONDS));
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        } catch (ExecutionException e)
        {
            e.printStackTrace();
        } catch (TimeoutException e)
        {
            e.printStackTrace();
        }
    }
}
