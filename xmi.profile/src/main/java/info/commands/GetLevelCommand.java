package info.commands;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.GenericCommand;
import ca.benpl.xmi.module.Module;
import info.runnables.GetLevel;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Ben on 2017-06-19.
 */
public class GetLevelCommand extends GenericCommand
{
    public GetLevelCommand(XMIBot xmi, Module parentModule)
    {
        super(xmi, parentModule);
        super.setName("level");
        super.setDescription("Usage: level <user> to get their current level.");
        super.setAliases(new ArrayList()
        {{
            add("lvl");
        }});
    }

    @Override
    public void execute(MessageEvent event)
    {
        Future<Integer> message = getBot().getExecutor().submit(new GetLevel(getBot().getDbClient(), event.getUser().getNick()));
        try
        {
            getBot().getIrcBot().send().message(event.getChannelSource(), String.valueOf(message.get(5, TimeUnit.SECONDS)));
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        } catch (ExecutionException e)
        {
            e.printStackTrace();
        } catch (TimeoutException e)
        {
            e.printStackTrace();
        }
    }
}
