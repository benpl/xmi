package info;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.module.AbstractModule;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import info.commands.*;

/**
 * Created by Ben on 23-May-17.
 */
public class InfoModule extends AbstractModule
{
    private StatefulRedisConnection connection;

    private InfoWriter infoWrite;
    private LevelTracker levelTrack;

    public static final String DB_NAME = "profile:";

    public InfoModule(final XMIBot xmi)
    {
        super(xmi);
    }

    @Override
    public void load()
    {
        super.addCommand(new GetProfileCommand(this.getBot(), this));
        super.addCommand(new SetInfoCommand(this.getBot(), this));
        super.addCommand(new SetTitleCommand(this.getBot(), this));
        super.addCommand(new GetLevelCommand(this.getBot(), this));
        super.addCommand(new GetXPCommand(this.getBot(), this));
        super.getBot().addIRCListener(new XPListener(this));
    }

    @Override
    public void unLoad()
    {
    }
}
