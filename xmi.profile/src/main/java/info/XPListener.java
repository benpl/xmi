package info;

import ca.benpl.xmi.module.Module;
import info.runnables.UpdateXP;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

/**
 * @author Ben Plante
 */
public class XPListener extends ListenerAdapter
{
    private final Module module;

    public XPListener(final Module module)
    {
        this.module = module;
    }

    public void onMessage(MessageEvent message)
    {
        if (!(message.getChannel() == null))
        {
            module.getBot().getExecutor().execute(new UpdateXP(module.getBot().getDbClient(), message.getUserHostmask().getNick()));
            module.getBot().getLogger().info("XP UPDATE");
        }
    }
}
