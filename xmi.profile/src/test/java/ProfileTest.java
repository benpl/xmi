import com.lambdaworks.redis.RedisClient;
import info.runnables.GetProfile;
import info.runnables.WriteInfo;
import info.runnables.WriteTitle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Ben on 2017-06-20.
 */
public class ProfileTest
{
    private final static String user = "tester";
    private final static String titleMessage = "title" + System.nanoTime();
    private final static String infoMessage = "message" + System.nanoTime();
    @Test
    public void testProfile()
    {

        RedisClient client = RedisClient.create("redis://localhost");
        new WriteInfo(client, user, infoMessage).run();
        new WriteTitle(client, user, titleMessage).run();
        Assert.assertEquals((user + ": " + titleMessage + "; " + infoMessage), new GetProfile(client, user).call());
    }
}
