package ca.benpl.xmi.command;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.List;

/**
 * Created by Ben on 16-May-17.
 */
public abstract class GenericCommand implements Command
{
    private final XMIBot xmi;
    private final Module parentModule;
    private boolean opPerms = false;
    private boolean enabled = true;

    private String name;
    private List<String> aliases;
    private String description;

    public GenericCommand(final XMIBot xmi, final Module parentModule)
    {
        this.xmi = xmi;
        this.parentModule = parentModule;
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    @Override
    public void setEnabled(boolean b)
    {
        enabled = b;
    }

    @Override
    public void requiresOp(boolean b)
    {
        opPerms = b;
    }

    @Override
    public boolean requiresOp()
    {
        return opPerms;
    }

    @Override
    public void execute()
    {

    }

    @Override
    public void execute(MessageEvent event)
    {

    }

    @Override
    public void execute(String... args)
    {

    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public List<String> getAliases()
    {
        return this.aliases;
    }

    @Override
    public void setAliases(List<String> aliases)
    {
        this.aliases = aliases;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }

    @Override
    public void setDescription(String description)
    {
        this.description = description;
    }

    public XMIBot getBot()
    {
        return this.xmi;
    }

    @Override
    public Module getParent()
    {
        return this.parentModule;
    }
}