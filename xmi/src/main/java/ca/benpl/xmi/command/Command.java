package ca.benpl.xmi.command;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.List;

/**
 * Created by Ben on 16-May-17.
 */
public interface Command
{
    String getName();

    boolean isEnabled();

    void setEnabled(boolean b);

    void setName(String name);

    List<String> getAliases();

    void setAliases(List<String> aliases);

    String getDescription();

    void setDescription(String description);

    XMIBot getBot();

    Module getParent();

    boolean requiresOp();

    void requiresOp(boolean b);

    void execute();

    void execute(MessageEvent event);

    void execute(String... args);
}
