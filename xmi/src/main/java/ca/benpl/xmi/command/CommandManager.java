package ca.benpl.xmi.command;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.module.Module;
import org.pircbotx.hooks.ListenerAdapter;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ben on 31-May-17.
 */
public class CommandManager extends ListenerAdapter
{
    private static Map<String, Command> commands = new HashMap<>();
    private final XMIBot xmi;
    private final Logger log;

    public CommandManager(final XMIBot xmi)
    {
        this.xmi = xmi;
        this.log = xmi.getLogger();
    }

    protected XMIBot getXmi()
    {
        return this.xmi;
    }

    protected Logger getLogger()
    {
        return this.log;
    }

    public Map<String, Command> getEnabledCommands()
    {
        return Collections.unmodifiableMap(commands);
    }

    public void registerCommands(Module module)
    {
        module.getSubCommands().forEach((String, Command) -> addCommand((Command) Command));
    }

    public void unregisterCommands(Module module)
    {
        module.getSubCommands().forEach((name, command) -> removeCommand((Command)command));
    }

    public void addCommand(Command command)
    {
        commands.put(command.getName(), command);
    }

    public void removeCommand(Command command)
    {
        commands.remove(command.getName());
    }

    public void setEnabledCommand(String command, boolean b)
    {
        if (commands.containsKey(command)) commands.get(command).setEnabled(b);
    }
}
