/*
 * Copyright (C) 2017 Ben Plante <ben.plante123@gmail.com>
 *
 * This file is part of XMIBot
 *
 * XMIBot is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * XMIBot is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * XMIBot. If not, see <http://www.gnu.org/licenses/>.
*/

package ca.benpl.xmi;

import ca.benpl.xmi.command.Command;
import ca.benpl.xmi.command.CommandManager;
import ca.benpl.xmi.module.Module;
import ca.benpl.xmi.module.ModuleManager;
import com.lambdaworks.redis.RedisClient;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * XMI is a modular and extensible implementation of {@link PircBotX}
 * that provides fun and useful functionality to users on an IRCServer.
 *
 * Modules of the bot can be added to the program dynamically and all share
 * a common structure so anyone can write a module for the bot and tailor it
 * to their needs. To optionally store data given to the program the bot uses
 * redis for storing and recalling information.
 *
 * By default the core and profile modules are included with the bot. The bot is
 * written to be an API so these modules can be optionally omitted if that is
 * preferred.
 *
 * @author Ben Plante
 * @version 0.9.1
 * @since  2017-06-22
 */

public class XMIBot
{
    // Required objects
    private final static Logger logger = LoggerFactory.getLogger(XMIBot.class);
    private final RedisClient dbClient;
    private PircBotX ircBot;

    // Managers
    private final ModuleManager modules;
    private final CommandManager commands;

    // Executors for queueing and scheduling tasks
    private ScheduledThreadPoolExecutor scheduler;
    private ThreadPoolExecutor executor;

    // Configuration objects for this object and the IRC bot
    private XConfiguration xConf;
    private Configuration pConf;

    // Modules to be loaded at startup
    private List<String> startModules = new ArrayList<>();

    public XMIBot()
    {
        // Create the managers
        modules = new ModuleManager(this);
        commands = new CommandManager(this);

        // Load the config file
        ConfigLoader loader;
        try
        {
            loader = new ConfigLoader();
            xConf = loader.getXMIConfig();
            pConf = loader.getPIRCConfig();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // Create the database client
        dbClient = RedisClient.create(xConf.getRedisURI());

        // Create the executors
        scheduler = new ScheduledThreadPoolExecutor(3);
        executor = new ThreadPoolExecutor(8, 20, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<>(30));

        // Add a graceful shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
        {
            logger.info("Init shutdown");

            logger.info("Stopping IRC Bot");
            ircBot.stopBotReconnect();
            ircBot.close();
            logger.info("Done!");

            logger.info("Unloading modules...");
            getModules().forEach((String, Module) -> modules.unLoadModule(String));
            logger.info("Done!");

            logger.info("Shutting down threads...");
            executor.shutdown();
            scheduler.shutdown();
            logger.info("Done!");

            logger.info("Shutting down redis...");
            dbClient.shutdown(1, 15, TimeUnit.SECONDS);
            logger.info("Done!");
        }));
    }

    /**
     * Queues modules to be loaded on bot start.
     *
     * @param args modules names
     */
    protected void defaultLoad(String... args)
    {
        startModules.addAll(Arrays.asList(args));
    }

    /**
     * Load all default modules and submit the IRCBot to the executor.
     */
    public void start()
    {
        // Submit PircBotX to the executor since startBot() blocks.
        executor.execute(() ->
        {
            try
            {
                startModules.forEach(modules::loadModule);
                this.ircBot = new PircBotX(pConf);
                ircBot.startBot();

            } catch (IOException | IrcException e)
            {
                e.printStackTrace();
            }
        });
    }

    /**
     * Close, reinitialize and resubmit the IRCBot to the executor.
     * Similar to start however it closes the connection and does
     * not load the default modules as they are already loaded.
     */
    public void restart()
    {

        ircBot.close();
        this.ircBot = new PircBotX(pConf);
        executor.execute(() ->
        {
            try
            {
                ircBot.startBot();
            } catch (IOException | IrcException e)
            {
                e.printStackTrace();
            }
        });
    }

    /**
     * Calls for the VM to exit gracefully which will trigger
     * the shutdown hook.
     */
    public void shutdown()
    {
        System.exit(0);
    }

    /**
     * Adds a new Listener object to the configuration used by the IRCBot. The
     * socket stays connected and a cycle is not required so loading modules that
     * specify listeners can essentially be silent.
     *
     * @see org.pircbotx.hooks.ListenerAdapter
     * @param listener Listener object
     */
    public void addIRCListener(Listener listener)
    {
        this.pConf = new Configuration.Builder(pConf).addListener(listener).buildConfiguration();
    }

    /**
     * Get the bot wide DataBase Client
     * @return the RedisClient
     */
    public RedisClient getDbClient()
    {
        return dbClient;
    }

    /**
     * Get the module manager used by this bot
     * @return the ModuleManager
     */
    public ModuleManager getModuleManager()
    {
        return modules;
    }

    /**
     * Get the command manager used by this bot
     * @return the CommandManager
     */
    public CommandManager getCommandManager()
    {
        return commands;
    }

    /**
     * Convenience method to get modules listed in the module manager
     * @return Map of Modules
     */
    public Map<String, Module> getModules()
    {
        return modules.getModules();
    }

    /**
     * Convenience method to get commands listed in the command manager
     * @return Map of Commands
     */
    public Map<String, Command> getCommands()
    {
        return commands.getEnabledCommands();
    }

    /**
     * Get the PircBotX client
     * @return the PircBotX
     */
    public PircBotX getIrcBot()
    {
        return ircBot;
    }

    /**
     * Get the bots logger
     * @return Logger
     */
    public Logger getLogger()
    {
        return logger;
    }

    /**
     * Get the scheduler to schedule tasks
     * @return Scheduler
     */
    public ScheduledThreadPoolExecutor getScheduler()
    {
        return scheduler;
    }

    /**
     * Get the executor to execute tasks
     * @return Executor
     */
    public ThreadPoolExecutor getExecutor()
    {
        return executor;
    }
}
