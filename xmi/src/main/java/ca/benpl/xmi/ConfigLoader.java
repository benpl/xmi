package ca.benpl.xmi;

import org.pircbotx.Configuration;
import org.pircbotx.UtilSSLSocketFactory;
import org.pircbotx.cap.TLSCapHandler;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by Ben on 16-May-17.
 */
public class ConfigLoader
{
    private FileReader configFile;
    Properties propConfig = new Properties();

    private XConfiguration xConfig;
    private Configuration pConfig;

    public ConfigLoader() throws IOException
    {
        this("config.properties");
    }

    public ConfigLoader(String configLocation) throws IOException
    {
        configFile = new FileReader(configLocation);
        loadConfig();
    }

    private void loadConfig() throws IOException
    {

        propConfig.load(configFile);

        XConfiguration.Builder xBuilder = new XConfiguration.Builder();
        xBuilder.setURI(propConfig.getProperty("RedisAddress", "redis://localhost"));

        Configuration.Builder pBuilder = new Configuration.Builder();
        pBuilder
                .addAutoJoinChannels(parseChannels())
                .addServers(parseServers())
                .setName(propConfig.getProperty("Nick", "XMI"))
                .setLogin(propConfig.getProperty("Login", "XMIBot"))
                .setRealName(propConfig.getProperty("RealName", "XMI Bot https://gitlab.com/benpl/xmi"));

        if (propConfig.containsKey("SSL")) if (Boolean.parseBoolean(propConfig.getProperty("SLL")))
        {
            SocketFactory sf = UtilSSLSocketFactory.getDefault();
            pBuilder.setSocketFactory(sf);
            pBuilder.addCapHandler(new TLSCapHandler((SSLSocketFactory)(sf), true));
        }

        if (propConfig.containsKey("AutoNickChange")) pBuilder.setAutoNickChange(Boolean.parseBoolean(propConfig.getProperty("AutoNickChange")));
        if (propConfig.containsKey("AutoReconnect")) pBuilder.setAutoReconnect(Boolean.parseBoolean(propConfig.getProperty("AutoReconnect")));
        if (propConfig.containsKey("AutoReconnectAttempts")) pBuilder.setAutoReconnectAttempts(Integer.parseInt(propConfig.getProperty("AutoReconnectAttempts")));
        if (propConfig.containsKey("AutoReconnectDelay")) pBuilder.setAutoReconnectDelay(Integer.parseInt(propConfig.getProperty("AutoReconnectDelay")));
        if (propConfig.containsKey("AutoSplitMessage")) pBuilder.setAutoSplitMessage(Boolean.parseBoolean(propConfig.getProperty("AutoSplitMessage")));
        if (propConfig.containsKey("CAP")) pBuilder.setCapEnabled(Boolean.parseBoolean(propConfig.getProperty("CAP")));
        if (propConfig.containsKey("ChannelPrefix")) pBuilder.setChannelPrefixes(propConfig.getProperty("ChannelPrefixes"));
        if (propConfig.containsKey("DCCAcceptTimeout")) pBuilder.setDccAcceptTimeout(Integer.parseInt(propConfig.getProperty("DCCAcceptTimeout")));
        if (propConfig.containsKey("DCCFilenameQuotes")) pBuilder.setDccFilenameQuotes(Boolean.parseBoolean(propConfig.getProperty("DCCFilenameQuotes")));
        if (propConfig.containsKey("DCCLocalAddress")) pBuilder.setDccLocalAddress(InetAddress.getByName(propConfig.getProperty("DCCLocalAddress")));
        if (propConfig.containsKey("DCCPassiveRequest")) pBuilder.setDccPassiveRequest(Boolean.parseBoolean(propConfig.getProperty("DCCPassiveRequest")));
        if (propConfig.containsKey("DCCPorts")) pBuilder.setDccPorts(parseDCCPorts());
        if (propConfig.containsKey("DCCPublicAddress")) pBuilder.setDccPublicAddress(InetAddress.getByName(propConfig.getProperty("DCCPublicAddress")));
        if (propConfig.containsKey("DCCResumeAcceptTimeout")) pBuilder.setDccResumeAcceptTimeout(Integer.parseInt(propConfig.getProperty("DCCResumeAcceptTimeout")));
        if (propConfig.containsKey("DCCTransferBufferSize")) pBuilder.setDccTransferBufferSize(Integer.parseInt(propConfig.getProperty("DCCTransferBufferSize")));
        if (propConfig.containsKey("Encoding")) pBuilder.setEncoding(Charset.forName(propConfig.getProperty("Encoding")));
        if (propConfig.containsKey("Finger")) pBuilder.setFinger(propConfig.getProperty("Finger"));
        if (propConfig.containsKey("IdentServerEnabled")) pBuilder.setIdentServerEnabled(Boolean.parseBoolean(propConfig.getProperty("IdentServerEnabled")));
        if (propConfig.containsKey("LocalAddress")) pBuilder.setLocalAddress(InetAddress.getByName(propConfig.getProperty("LocalAddress")));
        if (propConfig.containsKey("Locale")) pBuilder.setLocale(Locale.forLanguageTag(propConfig.getProperty("Locale")));
        if (propConfig.containsKey("MaxLineLength")) pBuilder.setMaxLineLength(Integer.parseInt(propConfig.getProperty("MaxLineLength")));
        if (propConfig.containsKey("MessageDelay")) pBuilder.setMessageDelay(Long.parseLong(propConfig.getProperty("MessageDelay")));
        if (propConfig.containsKey("NickServDelay")) pBuilder.setNickservDelayJoin(Boolean.parseBoolean(propConfig.getProperty("NickServDelay")));
        if (propConfig.containsKey("NickServNick")) pBuilder.setNickservNick(propConfig.getProperty("NickServNick"));
        if (propConfig.containsKey("NickServPassword")) pBuilder.setNickservPassword(propConfig.getProperty("NickServPassword"));
        if (propConfig.containsKey("NickServOnSuccess")) pBuilder.setNickservOnSuccess(propConfig.getProperty("NickServOnSuccess"));
        if (propConfig.containsKey("WhoOnJoin")) pBuilder.setOnJoinWhoEnabled(Boolean.parseBoolean(propConfig.getProperty("WhoOnJoin")));
        if (propConfig.containsKey("ShutdownHook")) pBuilder.setShutdownHookEnabled(Boolean.parseBoolean(propConfig.getProperty("ShutdownHook")));
        if (propConfig.containsKey("SocketTimeout")) pBuilder.setSocketTimeout(Integer.parseInt(propConfig.getProperty("SocketTimeout")));
        if (propConfig.containsKey("Snapshots")) pBuilder.setSnapshotsEnabled(Boolean.parseBoolean(propConfig.getProperty("Snapshots")));
        if (propConfig.containsKey("ServerPassword")) pBuilder.setServerPassword(propConfig.getProperty("ServerPassword"));
        if (propConfig.containsKey("UserLevelPrefixes")) pBuilder.setUserLevelPrefixes(propConfig.getProperty("UserLevelPrefixes"));
        if (propConfig.containsKey("Version")) pBuilder.setVersion(propConfig.getProperty("Version"));
        if (propConfig.containsKey("WebIRC")) pBuilder.setWebIrcEnabled(Boolean.parseBoolean(propConfig.getProperty("WebIRC")));
        if (propConfig.containsKey("WebIRCAddress")) pBuilder.setWebIrcAddress(InetAddress.getByName(propConfig.getProperty("WebIRCAddress")));
        if (propConfig.containsKey("WebIRCHostname")) pBuilder.setWebIrcHostname(propConfig.getProperty("WebIRCHostname"));
        if (propConfig.containsKey("WebIRCUsername")) pBuilder.setWebIrcUsername(propConfig.getProperty("WebIRCUsername"));
        if (propConfig.containsKey("WebIRCPassword")) pBuilder.setWebIrcPassword(propConfig.getProperty("WebPassword"));

        pConfig = pBuilder.buildConfiguration();
        xConfig = xBuilder.buildConfiguration();
    }

    private List<Integer> parseDCCPorts()
    {
        List<Integer> toReturn = new ArrayList<>();
        for (String entry : propConfig.getProperty("DCCPorts").split(", "))
        {
            toReturn.add(Integer.parseInt(entry));
        }
        return toReturn;
    }

    private List<String> parseChannels()
    {
        return Arrays.asList(propConfig.getProperty("Channels", "#XMI").split(", "));
    }

    private List<Configuration.ServerEntry> parseServers()
    {
        String[] serverArray = propConfig.getProperty("IRCServers", "127.0.0.1").split(", ");
        List<Configuration.ServerEntry> toReturn = new ArrayList<>();
        for (String entry : serverArray)
        {
            String host = entry.substring(0,entry.indexOf("/"));
            int port = Integer.parseInt(entry.substring(entry.indexOf("/")+1));
            toReturn.add(new Configuration.ServerEntry(host, port));
        }
        return toReturn;
    }

    public Configuration getPIRCConfig()
    {
        return pConfig;
    }

    public XConfiguration getXMIConfig()
    {
        return xConfig;
    }
}
