package ca.benpl.xmi;

/**
 * Based on the structure of {@link org.pircbotx.Configuration} XConfiguration
 * allows users to specify properties for the XMIBot.
 *
 * @author Ben Plante
 */
public class XConfiguration
{
    // Immutable configuration fields
    private final String redisURI;

    /**
     * Constructor for Configuration object. XMIBot refers to
     * the configuration object throughout runtime for various
     * parameters.
     *
     * @param builder Builder object
     */
    private XConfiguration(Builder builder)
    {
        this.redisURI = builder.redisURI;
    }

    /**
     * Builder pattern object to create a configuration object.
     * @param <T> Builder can be extended
     */
    public static class Builder <T extends Builder>
    {
        // configuration fields
        private String redisURI;

        /**
         * No Args constructor for a builder
         */
        public Builder()
        {}

        /**
         * Construct a builder from an existing builder
         * @param otherBuilder a Builder object
         */
        public Builder(Builder otherBuilder)
        {
            this.redisURI = otherBuilder.redisURI;
        }

        /**
         * Construct a builder from an existing configuration
         * @param configuration built configuration
         */
        public Builder(XConfiguration configuration)
        {
            this.redisURI = configuration.getRedisURI();
        }

        /**
         * Set the URI for redis to connect on
         * @param uri resolveable URI
         * @return Builder object to chain methods
         */
        public T setURI(String uri)
        {
            this.redisURI = uri;
            return (T) this;
        }

        /**
         * Build the configuration into a configuration object
         * @return XConfiguration with properties specified in the builder
         */
        public XConfiguration buildConfiguration()
        {
            return new XConfiguration(this);
        }
    }

    /**
     * Get the specified Redis Connection URI
     * @return RedisURI
     */
    public String getRedisURI()
    {
        return this.redisURI;
    }
}
