package ca.benpl.xmi.module;

import ca.benpl.xmi.XMIBot;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Ben on 08-Jun-17.
 */
public class ModuleLoader
{
    private final ModuleManager manager;
    private Map<String, URLClassLoader> moduleLoaders = new ConcurrentHashMap<>();


    protected ModuleLoader(final ModuleManager manager)
    {
        this.manager = manager;
    }

    /**
     * Called by load module to get the JSON file from the specified module.
     *
     * @param file Input JAR file
     * @return JSON file converted to a Module Data Object
     * @throws IOException If the JSON file is not in the top level directory of the JAR file
     */
    private static ModuleData loadJSONData(final ZipFile file) throws IOException
    {
        ZipEntry JSON = file.getEntry("module.json");

        return new Gson().fromJson(new InputStreamReader(file.getInputStream(JSON)), ModuleData.class);
    }

    public Map<String, URLClassLoader> getModuleLoaders()
    {
        return Collections.unmodifiableMap(moduleLoaders);
    }

    private Module loadModule(final File file, final ModuleData data) throws MalformedURLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {

        URLClassLoader loader = new URLClassLoader(new URL[]{file.toURI().toURL()});
        Class<?> mClass = null;
        try
        {
            mClass = Class.forName(data.getMClass(), true, loader);
        } catch (ClassNotFoundException e)
        {
            manager.getLogger().error("Could not load module at " + file.getPath() + ". The main class specified in its JSON file was not found!", e);
        }
        assert mClass != null;
        Module module  = (Module) mClass.getConstructor(XMIBot.class).newInstance(manager.getBot());
        moduleLoaders.put(data.getName(), loader);

        module.setData(data);

        return module;
    }

    public void loadModule(String... args)
    {
        File dir = new File("modules");

        for (String moduleName : args)
        {
            for (File file : dir.listFiles())
            {
                if (file.getName().endsWith(".jar"))
                {
                    try
                    {
                        ZipFile zip = new ZipFile(file);
                        ModuleData data = loadJSONData(zip);
                        if (data.getName().equalsIgnoreCase(moduleName))
                        {
                            try
                            {
                                Module module = loadModule(file, data);
                                manager.addModule(module);
                                manager.getLogger().info("Module JAR Loaded Successfully");
                            } catch (NoSuchMethodException ex)
                            {
                                manager.getLogger().error("Could not load module at " + file.getPath() + ". It's loader class does not implement the Module Interface!", ex);
                                return;
                            } catch (IllegalAccessException | InvocationTargetException | InstantiationException ex)
                            {
                                manager.getLogger().error("Could not load module at " + file.getPath() + ". There was an issue invoking or instantiating the loader class!", ex);
                                return;
                            }
                            return;
                        }
                        zip.close();
                    } catch (IOException ex)
                    {
                        manager.getLogger().error("Could not open " + file.getName() + " as a .jar", ex);
                        return;
                    }
                }
            }
        }
    }

    public void removeLoader(String moduleName)
    {
        moduleLoaders.remove(moduleName);
    }
}
