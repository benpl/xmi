/*
 * Copyright (C) 2017 Ben Plante <ben.plante123@gmail.com>
 *
 * This file is part of XMIBot
 *
 * XMIBot is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * XMIBot is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * XMIBot. If not, see <http://www.gnu.org/licenses/>.
*/

package ca.benpl.xmi.module;

import ca.benpl.xmi.XMIBot;

import java.util.Map;

/**
 * Interface for all modules.
 *
 * @author Ben Plante
 */
public interface Module
{
    XMIBot getBot();

    /**
     * Get the commands the map will specify
     * @return Map of the commands
     */
    Map getSubCommands();

    String getName();

    ModuleData getData();

    void setData(ModuleData data);

    void load();

    void unLoad();
}
