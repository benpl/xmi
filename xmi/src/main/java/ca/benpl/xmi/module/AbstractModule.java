package ca.benpl.xmi.module;

import ca.benpl.xmi.XMIBot;
import ca.benpl.xmi.command.Command;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ben on 31-May-17.
 */
public abstract class AbstractModule implements Module
{
    private final XMIBot xmi;
    ModuleData moduleData = null;
    Map<String, Command> subCommands = new HashMap<>();
    private String shortName;

    public AbstractModule(final XMIBot xmi)
    {
        this.xmi = xmi;
    }

    @Override
    public String getName()
    {
        return shortName;
    }

    public void setName(String name)
    {
        this.shortName = name;
    }

    @Override
    public ModuleData getData()
    {
        return this.moduleData;
    }

    @Override
    public void setData(ModuleData data)
    {
        moduleData = data;
        setName(data.getName());
    }

    @Override
    public Map<String, Command> getSubCommands()
    {
        return Collections.unmodifiableMap(subCommands);
    }

    public void addCommand(Command command)
    {
        subCommands.put(command.getName(), command);
    }

    @Override
    public void load()
    {

    }

    @Override
    public void unLoad()
    {

    }

    @Override
    public XMIBot getBot()
    {
        return this.xmi;
    }
}
