package ca.benpl.xmi.module;

import ca.benpl.xmi.XMIBot;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Ben on 30-May-17.
 */
public class ModuleManager
{
    private final XMIBot xmi;
    private final ModuleLoader loader;

    // Stores each module, the class loader for the module and its String name.
    // A map is used so that the name of the module can be reference later to
    // unload the module from the program by nullifying it.
    private Map<String, Module> loadedModules = new ConcurrentHashMap<>();

    public ModuleManager(final XMIBot xmi)
    {
        this.xmi = xmi;
        loader = new ModuleLoader(this);
    }

    public Logger getLogger()
    {
        return xmi.getLogger();
    }

    public XMIBot getBot()
    {
        return this.xmi;
    }

    public Map<String, Module> getModules()
    {
        return Collections.unmodifiableMap(loadedModules);
    }

    public void addModule(Module... modules)
    {
        for (Module module : modules)
        {
            loadedModules.put(module.getName(), module);

            module.load();
            xmi.getCommandManager().registerCommands(module);
            xmi.getLogger().info("Loaded Module " + module.getName());
        }
    }



    public void loadModule(String... args)
    {
        loader.loadModule(args);
    }

    /**
     * Calls a modules unload method to gracefully shut it down and then removes it from the Map of modules.
     * Then removes its class loader to dereference it and make it eligible for garbage collection.
     *
     * @param args Specified Modules
     */
    public void unLoadModule(String... args)
    {
        for (String moduleName : args)
        {
            if (loadedModules.containsKey(moduleName) && loader.getModuleLoaders().containsKey(moduleName))
            {
                loadedModules.get(moduleName).unLoad();
                getBot().getCommandManager().unregisterCommands(loadedModules.get(moduleName));
                loadedModules.remove(moduleName);
                loader.removeLoader(moduleName);

                // Call to garbage collector hoping that the both the objects have been dereferenced properly
                // and the garbage collector cares enough to remove them.
                System.gc();
                xmi.getLogger().info("Unloaded " + moduleName);
            } else if (loadedModules.containsKey(moduleName))
            {
                loadedModules.get(moduleName).unLoad();
                getBot().getCommandManager().unregisterCommands(loadedModules.get(moduleName));
                loadedModules.remove(moduleName);
                loader.removeLoader(moduleName);

                // Call to garbage collector hoping that the both the objects have been dereferenced properly
                // and the garbage collector cares enough to remove them.
                System.gc();
                xmi.getLogger().info("Unloaded " + moduleName);
            } else
            {
                xmi.getLogger().info("Specified module does not exist");
            }
        }
    }
}
