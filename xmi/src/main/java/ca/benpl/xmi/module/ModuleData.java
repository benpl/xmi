/*
 * Copyright (C) 2017 Ben Plante <ben.plante123@gmail.com>
 *
 * This file is part of XMIBot
 *
 * XMIBot is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * XMIBot is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * XMIBot. If not, see <http://www.gnu.org/licenses/>.
*/

package ca.benpl.xmi.module;

/**
 * Data file for a Module. To be loaded from JSON.
 * @author Ben Plante
 */
public class ModuleData
{
    private String name;
    private String mainClass;
    private String author;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getMClass()
    {
        return mainClass;
    }

    public void setMainClass(String mainClass)
    {
        this.mainClass = mainClass;
    }
}
