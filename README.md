# XMI - Extensible Modular IRC Bot
An IRC Bot designed from the ground up to dynamically load and unload modules. Modules can be written off of the framework provided here.
The bot can support multiple users on multiple channels and managers of the bot can write their own modules and load them into the program on the fly.

